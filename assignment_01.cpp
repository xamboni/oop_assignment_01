
/*---------------------------------------------------------------------
 *
 * assignment_01.cpp
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Description of assignment here
 * 
 * NOTE: Compile: `g++ -std=c++11 -o assignment_01 assignment_01.cpp`
 *
 ---------------------------------------------------------------------*/
 
/*---------------------------- Includes ------------------------------*/
 
 /* Standard */
#include <climits>
#include <cstdint>
#include <iostream>
#include <iomanip>

 /* User */
 
/*----------------------------- Globals ------------------------------*/

/*------------------------------ Types -------------------------------*/
 
/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

uint8_t swap_numbers_ptrs(uint32_t* first_number, uint32_t* second_number);
uint8_t swap_numbers_refs(uint32_t& first_number, uint32_t& second_number);
uint32_t print_sizes(void);

/*------------------------- Implementation ---------------------------*/
 
 int main (int argc, char* argv[])
{	 
 /*
  * Inputs: 
  * Outputs: 
  * Note: 
  * 
----------------------------------------------------------------------*/

    uint32_t result = false;

    // Problem 1.
    result = print_sizes();

    // Problem 2.
    uint32_t first_number = 10;
    uint32_t second_number = 5;

    std::cout << "Before: " << first_number << " " << second_number << std::endl;
    result = swap_numbers_ptrs(&first_number, &second_number);
    std::cout << "After: " << first_number << " " << second_number << std::endl;

    first_number = 10;
    second_number = 5;
    std::cout << "Before: " << first_number << " " << second_number << std::endl;
    result = swap_numbers_refs(first_number, second_number);
    std::cout << "After: " << first_number << " " << second_number << std::endl;

}

uint8_t swap_numbers_ptrs(uint32_t* first_number, uint32_t* second_number)
{
    if (*second_number < *first_number)
    {
        uint32_t temp_number = *first_number;

        *first_number = *second_number;
        *second_number = temp_number;
    }

    return 0;
}

uint8_t swap_numbers_refs(uint32_t& first_number, uint32_t& second_number)
{
    if (second_number < first_number)
    {
        uint32_t temp_number = first_number;

        first_number = second_number;
        second_number = temp_number;
    }

    return 0;
}

//CHAR_BIT,
//CHAR_MAX,
//CHAR_MIN,
//SHRT_MAX,
//SHRT_MIN,
//USHRT_MAX,
//INT_MAX,
//INT_MIN,
//UINT_MAX,
//LONG_MAX,
//LONG_MIN,
//ULONG_MAX,
//LLONG_MAX,
//LLONG_MIN,
//ULLONG_MAX

uint32_t print_sizes(void)
{
    // CHAR_BIT
    std::cout << "CHAR_BIT: " << CHAR_BIT << std::endl;
    std::cout << "CHAR_BIT size: " << sizeof(CHAR_BIT) << std::endl;

    // CHAR_MIN && CHAR_MAX
    std::cout << "CHAR_MIN: " << CHAR_MIN << " CHAR_MAX: " << CHAR_MAX << std::endl;
    std::cout << "CHAR_MIN size: " << sizeof(CHAR_MIN) << " CHAR_MAX size: " << sizeof(CHAR_MAX) << std::endl;

    // SHRT_MIN && SHRT_MAX
    std::cout << "SHRT_MIN: " << SHRT_MIN << " SHRT_MAX: " << SHRT_MAX << std::endl;
    std::cout << "SHRT_MIN size: " << sizeof(SHRT_MIN) << " SHRT_MAX size: " << sizeof(SHRT_MAX) << std::endl;

    // USHRT_MAX
    std::cout << "USHRT_MAX: " << USHRT_MAX << std::endl;
    std::cout << "USHRT_MAX size: " << sizeof(USHRT_MAX) << std::endl;

    // INT_MIN && INT_MAX
    std::cout << "INT_MIN: " << INT_MIN << " INT_MAX: " << INT_MAX << std::endl;
    std::cout << "INT_MIN size: " << sizeof(INT_MIN) << " INT_MAX size: " << sizeof(INT_MAX) << std::endl;

    // UINT_MAX
    std::cout << "UINT_MAX: " << UINT_MAX << std::endl;
    std::cout << "UINT_MAX size: " << sizeof(UINT_MAX) << std::endl;

    // LONG_MIN && LONG_MAX
    std::cout << "LONG_MIN: " << LONG_MIN << " LONG_MAX: " << LONG_MAX << std::endl;
    std::cout << "LONG_MIN size: " << sizeof(LONG_MIN) << " LONG_MAX size: " << sizeof(LONG_MAX) << std::endl;

    // ULONG_MAX
    std::cout << "ULONG_MAX: " << ULONG_MAX << std::endl;
    std::cout << "ULONG_MAX size: " << sizeof(ULONG_MAX) << std::endl;

    // LLONG_MIN && LLONG_MAX
    std::cout << "LLONG_MIN: " << LLONG_MIN << " LLONG_MAX: " << LLONG_MAX << std::endl;
    std::cout << "LLONG_MIN size: " << sizeof(LLONG_MIN) << " LLONG_MAX size: " << sizeof(LLONG_MAX) << std::endl;

    // ULLONG_MAX
    std::cout << "ULLONG_MAX: " << ULLONG_MAX << std::endl;
    std::cout << "ULLONG_MAX size: " << sizeof(ULLONG_MAX) << std::endl;

    return 0;
}
